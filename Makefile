all: emake dialyzer

emake:
	erl -make

dialyzer:
	dialyzer --src src

runlocal:
	zxh runlocal --libs=wfc:../wfc/
