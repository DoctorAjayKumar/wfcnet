# WFCNet

WFCNet is a telnet front-end for WFC, the WF compiler. WFC simplifies
expressions in WF algebra to canonical WF polynumbers. Eventually it
will do a lot more, but that's what it does right now.

This document explains how to use WFCNet, but doesn't explain the
internals of WFC or how WF algebra works. See the demo playlist for
that.

- YouTube WFC demo playlist: (tba)
- WFC: https://gitlab.com/DoctorAjayKumar/wfc/

WF algebra was developed by Norman J. Wildberger. It is an improved
version of Boolean algebra. Prof. Wildberger calls WF algebra
"Algebra of Boole". WF algebra is a much better name. You can see
Prof. Wildberger's playlist explaining it here:

- https://www.youtube.com/playlist?list=PLIljB45xT85CnIGIWb7tH1F_S2PyOC8rb

To use WFCNet, simply run

```
$ telnet orangepill.healthcare 2363
```

- `w` is the `23`rd letter of the alphabet
- `f` is the `6`th letter of the alphabet
- `c` is the `3`rd letter of the alphabet

Please report bugs in the GitLab bug tracker or email The Founder.

### Basic help

When you run that command, this is what happens:

```
$ telnet orangepill.healthcare 2363
Trying 143.198.109.198...
Connected to orangepill.healthcare.
Escape character is '^]'.
---
Welcome to WFCNet, a telnet frontend to WFC, the WF compiler.
Written by Dr. Ajay Kumar PHD, The Founder.
Documentation: https://gitlab.com/DoctorAjayKumar/wfcnet/-/blob/master/README.md
Please report bugs on GitLab or to DoctorAjayKumar@protonmail.com

- You can run (help) at any time to see this message.
- You can run (connectives) at any time to see the list of connectives.
- You can run (halt) at any time to quit.

- WF algebra is an improved form of Boolean algebra.
- This program reduces WF algebra expressions to canonical form.
- That canonical form is an integers-mod-2 polynomial written in sum-of-products form.
- For instance:
      > (and a b)
      (+ (* a b))
      > (xor a b)
      (+ (* a) (* b))
- The empty product is 1 (true) and the empty sum is 0 (false)
- This is fake Lisp. The calling syntax is S-Expressions.  Eventually it will be a less fake Lisp, and I will add readline support.
- Valid polynomial variables are one or more lowercase English letters.
- There is no state.
- Errors print to the console.

Try some simple expressions like
      > (implies a b)
      > (and (implies a b) (implies b c))
      > (implies (and (implies a b) (implies b c)) (implies a c))
---
>
```

### Connectives

If you run `(connectives)`, this is the message that prints

```
> (connectives)
% From:
% https://gitlab.com/DoctorAjayKumar/wfc/-/blob/master/src/wfc_strparse.erl#L116
% the ones that mean and
eval_op('*') -> fun wfc_listfuns:land/1;
eval_op("and") -> fun wfc_listfuns:land/1;
eval_op("intersection") -> fun wfc_listfuns:land/1;
% the ones that mean "iff"
eval_op("iff") -> fun wfc_listfuns:liff/1;
eval_op("eqset") -> fun wfc_listfuns:liff/1;
% the ones that mean "implies"
eval_op("implies") -> fun wfc_listfuns:limplies/1;
eval_op("subset") -> fun wfc_listfuns:limplies/1;
% the ones that mean "impliedby"
eval_op("impliedby") -> fun wfc_listfuns:limpliedby/1;
eval_op("supset") -> fun wfc_listfuns:limpliedby/1;
% the ones that mean "ior"
eval_op("ior") -> fun wfc_listfuns:lior/1;
eval_op("union") -> fun wfc_listfuns:lior/1;
% the ones that mean not
eval_op("not") -> fun wfc_listfuns:lnot/1;
eval_op("complement") -> fun wfc_listfuns:lnot/1;
% the ones that mean xor
eval_op('+') -> fun wfc_listfuns:lxor/1;
eval_op("symdiff") -> fun wfc_listfuns:lxor/1;
eval_op("xor") -> fun wfc_listfuns:lxor/1.
```

Currently there are 7 connectives: `and`, `iff`, `implies`,
`impliedby` (just an argflip of implies), `ior`, `not`, and `xor`.

Those listfuns are defined as follows

```erlang
% https://gitlab.com/DoctorAjayKumar/wfc/-/blob/master/src/wfc_listfuns.erl
land(List) -> wfc:prod(List).
liff([A, B])       -> wfc:wiff(A, B).
limpliedby([A, B]) -> wfc:wimpliedby(A, B).
limplies([A, B])   -> wfc:wimplies(A, B).

lior(List) ->
    lists:foldl(fun wfc:wior/2, zero(), List).

lnot([X])  -> wfc:wnot(X).
lxor(List) -> wfc:sum(List).
```

Thus,

- `iff`, `impliedby`, `implies` are fussy in that they allow exactly
  two operands
- `not` is fussy in that it only allows one operand
- all other connectives allow an arbitrary number of operands

### Magic values

There are two special values: `0` and `1`. Currently these are the
only values that are allowed as naked expressions

```
> 0
(+)
> 1
(+ (*))
> (+ 0 1)
(+ (*))
> (and 0 1)
(+)
```

Remember,

- `+` is a synonym for `xor`. Write out
    - an addition table mod 2 for the former, and
    - a truth table for the latter
    - to see why.
    - `symdiff` (the set operation "symmetric difference") is also a synonym.
- `*` is a synonym for `and` and `intersection`, for similar reasons
- 0 is the empty sum
- 1 is the empty product
